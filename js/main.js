$(document).ready(function() {
    var Madagaskar = { // EPSG:3857
        lat: -20.396123272467616,
        lng: 48.328857421875,
        zoom: 5
    };
    map.init('default', Madagaskar.lat, Madagaskar.lng, Madagaskar.zoom);
    mapChooser.init();
});

var map = {};

map.constructOLLayer = function(layerKey){
    switch(layerKey) {
        case 'osm':
            return new ol.layer.Tile({
                source: new ol.source.OSM(),
                visible: true
            });
        case 'bing':
            return new ol.layer.Tile({
                visible: true,
                preload: Infinity,
                source: new ol.source.BingMaps({
                    key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
                    imagerySet: 'AerialWithLabels'
                })
            });
        default: // default layer (openlayers)
            return new ol.layer.Tile({
                source: new ol.source.MapQuest({layer: 'sat'}),
                visible: true
            })
    }
};

map.constractMap = function(layerKey, lat, lng, zoom) {
    layerKey = layerKey || 'default';
    lat = lat || 0;
    lng = lng || 0;
    zoom = zoom || 2;

    $('#map').empty();

    if (layerKey === 'gm') {
        map.obj = new GMaps({
            div: '#map',
            lat: lat,
            lng: lng,
            zoom: zoom
        });
    } else {
        map.obj = new ol.Map({
            target: 'map',
            layers: [map.constructOLLayer(layerKey)],
            view: new ol.View({
                center: [lng, lat],
                zoom: zoom
            })
        });
    }
};

map.getMapInfo = function(targetLayerKey, currentLayerKey) {
    var coordinates,
        zoom;

    if(currentLayerKey === 'gm') {
        coordinates = map.obj.getCenter();
        coordinates = [ // EPSG:4326
            coordinates.B, // lat
            coordinates.k // lng
        ];
        zoom = map.obj.getZoom();
    } else {
        var coordinatesPx = map.obj.getCoordinateFromPixel([$(window).width()/2, $(window).height()/2]);
        coordinates = [ // EPSG:3857
            coordinatesPx[0], // lat
            coordinatesPx[1] // lng
        ];
        zoom = map.obj.getView().getZoom();
    }

    if(currentLayerKey === 'gm' && targetLayerKey !== 'gm') {
        // EPSG:4326 => EPSG:3857
        coordinates = ol.proj.transform([coordinates[0], coordinates[1]], 'EPSG:4326','EPSG:3857');
    }

    if(currentLayerKey !== 'gm' && targetLayerKey === 'gm') {
        // EPSG:3857 => EPSG:4326
        coordinates = ol.proj.transform([coordinates[0], coordinates[1]], 'EPSG:3857', 'EPSG:4326');
    }

    return {
        lng: coordinates[0],
        lat: coordinates[1],
        zoom: zoom
    };
};

map.init = function(layerKey, lat, lng, zoom) {

    if(layerKey !== 'gm') {
        var coordinates = ol.proj.transform([lng, lat], 'EPSG:4326', 'EPSG:3857');
        lng = coordinates[0];
        lat = coordinates[1];
    }

    map.constractMap(layerKey, lat, lng, zoom);

    $('.js-export').click(function(e) {
        var _link = this;

        var chooserInfo = mapChooser.getElInfo();
        var layerKey = chooserInfo.layerKey;

        if(layerKey === 'gm') {
            var mapInfo = map.getMapInfo(layerKey, 'gm');
            var lat = mapInfo.lat;
            var lng = mapInfo.lng;
            var zoom = mapInfo.zoom;

            var $win = $(window);
            _link.href = GMaps.staticMapURL({
                size: [$win.width(), $win.height()],
                lat: lat,
                lng: lng,
                zoom: zoom
            });
        } else {
            map.obj.once('postcompose', function (event) {
                var canvas = event.context.canvas;
                _link.href = canvas.toDataURL('image/png');
            });
            map.obj.renderSync();
        }
    });
};

var mapChooser = {};
mapChooser.init = function () {
    $('.js-map-chooser a').click(function (e) {
        e.preventDefault();

        var targetElInfo = mapChooser.getElInfo($(this));
        var targetLayerKey = targetElInfo.layerKey;
        var targetBtnPosition = targetElInfo.btnPosition;

        var currentElInfo = mapChooser.getElInfo($('.js-map-chooser a.js-current'));
        var currentLayerKey = currentElInfo.layerKey;

        if (targetBtnPosition === 'pos-base') {
            mapChooser.changeBtnVis();
            return;
        }

        mapChooser.changeMapLayer(targetLayerKey, currentLayerKey);

        $('.js-current')
            .removeClass('js-current pos-base')
            .addClass(targetBtnPosition);

        $(this)
            .removeClass(targetBtnPosition)
            .addClass('js-current pos-base');

        mapChooser.changeBtnVis();
    });

    $('.js-overlay').click(function (e) {
        e.preventDefault();
        mapChooser.changeBtnVis(true);
        $(this).hide();
    });

    mapChooser.changeBtnVis(true);
};

mapChooser.getElInfo = function($el) {
    $el = $el || $('.js-map-chooser a.js-current');

    var classes = $el.attr('class').split(' '),
        mapRe = /^js-map-(\w+)$/g,
        positionRe = /^(pos-\w+)$/g,
        result = {};

    for (var n = 0, len = classes.length; n < len; n++) {
        if (mapRe.test(classes[n])) {
            result.layerKey = RegExp.$1;
        }

        if (positionRe.test(classes[n])) {
            result.btnPosition = RegExp.$1;
        }
    }
    return result;
};

mapChooser.changeBtnVis = function (hide) {
    var btns = $('.js-map-chooser a').not(".js-current");
    if (hide) {
        btns.hide();
        $('.js-overlay').hide();
    } else {
        btns.toggle();
        $('.js-overlay').toggle();
    }
};

mapChooser.changeMapLayer = function(toLayerKey, fromLayerKey){
    var mapInfo = map.getMapInfo(toLayerKey, fromLayerKey);

    map.constractMap(toLayerKey, mapInfo.lat, mapInfo.lng, mapInfo.zoom);
};